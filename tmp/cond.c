#include <stdio.h>

int printd( int i );

int main() {
  int i,j;
  i = 450;
  j = -123;

  if ( i+1 < j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 < 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 < j ) printf("%d",i); else printf("%d",j);
  if ( 45 < j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 < 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 < j ) printf("%d",i); else printf("%d",j);
  if ( i < j+0 ) printf("%d",i); else printf("%d",j);
  if ( i < 123 ) printf("%d",i); else printf("%d",j);
  if ( i < j ) printf("%d",i); else printf("%d",j);

  if ( i+1 > j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 > 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 > j ) printf("%d",i); else printf("%d",j);
  if ( 45 > j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 > 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 > j ) printf("%d",i); else printf("%d",j);
  if ( i > j+0 ) printf("%d",i); else printf("%d",j);
  if ( i > 123 ) printf("%d",i); else printf("%d",j);
  if ( i > j ) printf("%d",i); else printf("%d",j);

  if ( i+1 >= j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 >= 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 >= j ) printf("%d",i); else printf("%d",j);
  if ( 45 >= j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 >= 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 >= j ) printf("%d",i); else printf("%d",j);
  if ( i >= j+0 ) printf("%d",i); else printf("%d",j);
  if ( i >= 123 ) printf("%d",i); else printf("%d",j);
  if ( i >= j ) printf("%d",i); else printf("%d",j);

  if ( i+1 <= j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 <= 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 <= j ) printf("%d",i); else printf("%d",j);
  if ( 45 <= j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 <= 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 <= j ) printf("%d",i); else printf("%d",j);
  if ( i <= j+0 ) printf("%d",i); else printf("%d",j);
  if ( i <= 123 ) printf("%d",i); else printf("%d",j);
  if ( i <= j ) printf("%d",i); else printf("%d",j);

  if ( i+1 == j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 == 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 == j ) printf("%d",i); else printf("%d",j);
  if ( 45 == j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 == 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 == j ) printf("%d",i); else printf("%d",j);
  if ( i == j+0 ) printf("%d",i); else printf("%d",j);
  if ( i == 123 ) printf("%d",i); else printf("%d",j);
  if ( i == j ) printf("%d",i); else printf("%d",j);

  if ( i+1 != j+0 ) printf("%d",i); else printf("%d",j);
  if ( i+1 != 123 ) printf("%d",i); else printf("%d",j);
  if ( i+1 != j ) printf("%d",i); else printf("%d",j);
  if ( 45 != j+0 ) printf("%d",i); else printf("%d",j);
  if ( 45 != 123 ) printf("%d",i); else printf("%d",j);
  if ( 45 != j ) printf("%d",i); else printf("%d",j);
  if ( i != j+0 ) printf("%d",i); else printf("%d",j);
  if ( i != 123 ) printf("%d",i); else printf("%d",j);
  if ( i != j ) printf("%d",i); else printf("%d",j);

  return 0;
}
