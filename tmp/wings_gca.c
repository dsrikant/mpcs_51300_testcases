/*
an example implementation of get_char_at using only defined primitives of the CSTR language

performs iterative-deepening depth-first-search of the space of all strings from a limited charset

in this case, the charset is [ 0-9a-z] but this is easy to change in the calls to the CHECK and CHECK_ASSUME macros

indexing runs in exponential time on the length of the string

run this code through the C preprocessor with `cpp -U__GNUC__ < gca.c > gca.c.pp` to emit code suitable for a CSTR compiler, or run it through gcc normally
*/

#ifdef __GNUC__

#include <stdio.h>
#include <string.h>
#include <glib.h>

char retbuf[128];

#define printf2 printf
#define set(x, y) (*x=0, strcpy(x, y))
//#define add(x, y) g_strconcat(x, y, NULL)
#define add(out, x, y) (set(out, x), strcat(out, y), out)
#define eq(x, y) (!g_strcmp0(x, y))

typedef char* string;
typedef char mkstring[128];

#else

#define mkstring string
#define printf2(...)
#define set(x, y) x=y
#define add(out, x, y) out=x+y
#define eq(x, y) x==y

extern int printf(string s); 

#endif

string gca_len(string s, int length, string prefix, int i, string answer, int depth)
{
	mkstring prefix2;
	string x;
	x="";

	if(depth==0)
	{
		return "";
	}

	printf2("gca_len('%s', %d, '%s', %d, '%s')\n", s, length, prefix, i, answer);
	if(length>14)
	{
		printf2("out of length\n");
		return "";
	}

	if(eq(s, prefix))
	{
		return answer;
	}

	if(i==0)
	{
		#define CHECK_ASSUME(c) \
		if(eq(x, ""))\
		{\
			add(prefix2, prefix, (c));\
			x=gca_len(s, length+1, prefix2, i-1, c, depth-1);\
		}
		CHECK_ASSUME(" ")
		CHECK_ASSUME("0")
		CHECK_ASSUME("1")
		CHECK_ASSUME("2")
		CHECK_ASSUME("3")
		CHECK_ASSUME("4")
		CHECK_ASSUME("5")
		CHECK_ASSUME("6")
		CHECK_ASSUME("7")
		CHECK_ASSUME("8")
		CHECK_ASSUME("9")
		CHECK_ASSUME("a")
		CHECK_ASSUME("b")
		CHECK_ASSUME("c")
		CHECK_ASSUME("d")
		CHECK_ASSUME("e")
		CHECK_ASSUME("f")
		CHECK_ASSUME("g")
		CHECK_ASSUME("h")
		CHECK_ASSUME("i")
		CHECK_ASSUME("j")
		CHECK_ASSUME("k")
		CHECK_ASSUME("l")
		CHECK_ASSUME("m")
		CHECK_ASSUME("n")
		CHECK_ASSUME("o")
		CHECK_ASSUME("p")
		CHECK_ASSUME("q")
		CHECK_ASSUME("r")
		CHECK_ASSUME("s")
		CHECK_ASSUME("t")
		CHECK_ASSUME("u")
		CHECK_ASSUME("v")
		CHECK_ASSUME("w")
		CHECK_ASSUME("x")
		CHECK_ASSUME("y")
		CHECK_ASSUME("z")
		#undef CHECK_ASSUME
	}
	else
	{
		#define CHECK(c) \
		if(eq(x, ""))\
		{\
			add(prefix2, prefix, (c));\
			x=gca_len(s, length+1, prefix2, i-1, answer, depth-1);\
		}
		CHECK(" ")
		CHECK("0")
		CHECK("1")
		CHECK("2")
		CHECK("3")
		CHECK("4")
		CHECK("5")
		CHECK("6")
		CHECK("7")
		CHECK("8")
		CHECK("9")
		CHECK("a")
		CHECK("b")
		CHECK("c")
		CHECK("d")
		CHECK("e")
		CHECK("f")
		CHECK("g")
		CHECK("h")
		CHECK("i")
		CHECK("j")
		CHECK("k")
		CHECK("l")
		CHECK("m")
		CHECK("n")
		CHECK("o")
		CHECK("p")
		CHECK("q")
		CHECK("r")
		CHECK("s")
		CHECK("t")
		CHECK("u")
		CHECK("v")
		CHECK("w")
		CHECK("x")
		CHECK("y")
		CHECK("z")
		#undef CHECK
	}
	return x;
}

string get_char_at(string s, int i)
{
	string x;
	int depth;
	x="";
	depth=1;

	if(i > 128)
	{
		return "";
	}
	else
	{
		while(x=="")
		{
			x=gca_len(s, 0, "", i, "dunno yet", depth);
			depth=depth+1;
		}
		return x;
	}
}

#ifdef __GNUC__
int main(int argc, string* argv)
#else
int main()
#endif
{
#ifdef __GNUC__
	int i=0;
	while(argv[1][i])
	{
		printf2("gca(x, %d)='%s'\n", i, get_char_at(argv[1], i));
		i++;
	}
#else
	printf(get_char_at("abc0",0));
	printf(get_char_at("abc0",1));
	printf(get_char_at("abc0",2));
	printf(get_char_at("abc0",3));
#endif
}
