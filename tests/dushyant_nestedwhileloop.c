int printd( int i );

int main() {
  int i;
  i = 0;
  while ( i < 10 ) {
    int j;
    j = i;
    while ( j < 5 ) {
      j = j + 1;
      printd(j);
    }
    printd(i);
    i = i + 2;
  }
  return 0;
}
